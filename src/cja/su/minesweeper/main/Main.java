package cja.su.minesweeper.main;

import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // size of field
        int gridSize = 10;

        // The grafic looks better
        SwingUtilities.invokeLater(() -> Minesweeper.run(gridSize));
    }

    static void openDialog(JFrame frame, String message, String title) {
        JOptionPane.showMessageDialog(frame, message, title, JOptionPane.ERROR_MESSAGE);
    }

}